<?php

  /**
   * This plugin array is more or less self documenting
   */
  $plugin = array(
    // the title in the admin
    'title' => t('Previous Next Navigation pane'),
    // no one knows if "single" defaults to FALSE...
    'single' => TRUE,
    // oh joy, I get my own section of panel panes
    'category' => array(t('Navigation'), -9),
    'render callback' => 'prevnextpane_pane_content_type_render'
  );

  /**
   * Run-time rendering of the body of the block (content type)
   * See ctools_plugin_examples for more advanced info
   */
  function prevnextpane_pane_content_type_render($subtype, $conf, $context = NULL) {
    $block = new stdClass();
    $nid = $context[0];


    $nids = _prevnextpane_get_nids();
    $links = _prevnextpane_prev_next_links($nids, $nid);
    $next_title = prevnextpane_load_node_title($links['next_nid']);
    $prev_title = prevnextpane_load_node_title($links['prev_nid']);
    $prev_node = node_load($links['prev_nid']);
    $img = prevnextpane_prev_next_image($prev_node);
    $previous = empty($links['prev_nid']) ? \NULL : l($img . $prev_title, 'node/' . $links['prev_nid'], array('html' => TRUE, 'attributes' => array('class' => array('prev-next-api-anchor-link'))));

    $next_node = node_load($links['next_nid']);
    $img = prevnextpane_prev_next_image($next_node);
    $next = empty($links['next_nid']) ? \NULL : l($img . $next_title, 'node/' . $links['next_nid'], array('html' => TRUE, 'attributes' => array('class' => array('prev-next-api-anchor-link'))));
    $block->content = theme('prevnextpane', array('prev' => $previous, 'next' => $next));
    return $block;
  }

  /*
   * @function Utility function to return node title without having to load the entire node
   * @param $nid  the node id 
   * @return The node title as string or empty string
   */

  function prevnextpane_load_node_title($nid) {
    $title = '';
    $query = db_select('node', 'n');
    $query->fields('n', array('title'));
    $query->condition('n.nid', $nid, '=');
    $result = $query->execute();
    foreach ($result as $record) {
      // Do something with each $record
      $title = $record->title;
    }
    return $title;
  }

  /*
   * helper function to get the proper image form old or new blog content type
   * @param node -  an old or new style blog node
   * @return string containing image markup or empty string
   */
  function prevnextpane_prev_next_image($node) {
    $img = '';
    if ($node->type === 'blog_post') {
      if (!empty($node->field_blog_teaser_image_image[LANGUAGE_NONE][0]['uri'])) {
        $img = theme('image_style', array('style_name' => 'width272', 'path' => $node->field_blog_teaser_image_image[LANGUAGE_NONE][0]['uri'], 'attributes' => array('class' => 'prev_img'),));
      }
    }
    else {
      if (!empty($node->field_episode_header_image_image[LANGUAGE_NONE][0]['uri'])) {
        $img = theme('image_style', array('style_name' => 'width272', 'path' => $node->field_episode_header_image_image[LANGUAGE_NONE][0]['uri'], 'attributes' => array('class' => 'prev_img'),));
      }
    }
    return $img;
  }

  